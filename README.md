# RobosNaEscola

Repositório para agrupar todo o material público desenvolvido para expandir o projeto de inclusão digital nas escolas públicas de São Carlos.


## Grupo de Projetos

Tem como função acompanhar as demais equipes para verificar o andamento do trabalho e o progresso das atividades. Busca manter tudo dentro do prazo, assim como avaliar a possibilidade de expansão para outros projetos. Fazendo parte desse grupo, você irá aprender a gerenciar pessoas, assim como trabalhar em equipe. Você se tornará mais atento ao prazos e datas e uma pessoa mais pró-ativa, habilidades que serão um diferencial no futuro.

## Grupo de Recursos Humanos

Tem como função gerenciar os recursos humanos do projeto e garantir o bem estar de seus participantes. Fazendo parte desse grupo, você pode aprender coisas como gerenciamento de equipes, trabalho em equipe e captação de voluntários.

## Grupo de Desenvolvimento 

Tem a função de prover e manter todos os robôs e software relacionados ao projeto, sendo então um dos grupos de vital importância para todo o projeto. Como projeto de longo prazo, planejamos desenvolver um robô que possa ser usado para ensinar robótica, à um baixo custo e de baixa complexidade, sendo esse escalável para podermos distribuí-lo por São Carlos e, eventualmente, outros municípios conforme o crescimento do projeto. Fazendo parte desse grupo, você terá contato com Arduino e eletrônica, oportunidade de trabalhar com hardware e software, na correção de problemas e elaboração de novos sistemas.

## Grupo de Atividades

Tem como função fazer as atividades/exercícios/jogos que as crianças vão participar/jogar durante as aulas, para fixar os conteúdos, os que usam robôs e os que não usam. Fazendo parte desse grupo, você terá um maior contato com técnicas de pedagogia e game design, podendo assim, aprimorar suas habilidades de didática e de criar tarefas que despertem a curiosidade nas crianças.

## Grupo de Marketing

Tem como função difundir as ideias do projeto utilizando redes sociais e anúncios. Fazendo parte desse grupo, você vai adquirir uma grande base no meio da comunicação, sendo essa a melhor forma de chegar ao cliente final, habilidade que será um diferencial no futuro.
