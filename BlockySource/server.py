#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# credit: http://sheep.art.pl/Wiki%20Engine%20in%20Python%20from%20Scratch


import BaseHTTPServer
import SimpleHTTPServer
import itertools
import logging
import platform
import os
import re
import subprocess
import tempfile
import urllib
from optparse import OptionParser


logging.basicConfig(level=logging.DEBUG)

last = 0

class Handler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_HEAD(self):
        """Send response headers"""
        if self.path != "/":
            return SimpleHTTPServer.SimpleHTTPRequestHandler.do_HEAD(self)
        self.send_response(200)
        self.send_header("content-type", "text/html;charset=utf-8")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()

    def do_GET(self):
        """Send page text"""
        for i in range(30):
            if self.path == "/"+str(i):
                self.send_response(302)
                self.send_header("Location","./uploads/"+str(i)+".ino")
                self.end_headers()
                return 
        if self.path != "/":
            return SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)
        else:
            self.send_response(302)
            self.send_header("Location", "/blockly/apps/blocklyduino/index.html")
            self.end_headers()

    def do_POST(self):

        partes = self.path.split('/')
        nome = partes[2]
        print partes

        length = int(self.headers.getheader('content-length'))
        if length:
            text = self.rfile.read(length)
            global last
            f = open("./uploads/"+str(last)+".ino", "wb")
            f.write(text + "\n")
            f.close()
            f = open("./uploads/log","a")
            f.write(str(last) + ":" + nome+"\n")
            f.close()
            last += 1
            if last >= 3000:
              last = 0
        else:
            self.send_response(400)


if __name__ == '__main__':
    server = BaseHTTPServer.HTTPServer(("143.107.183.170", 8080), Handler)
    server.pages = {}
    server.serve_forever()
